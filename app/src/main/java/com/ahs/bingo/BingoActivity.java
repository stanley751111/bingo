package com.ahs.bingo;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BingoActivity extends BaseActivity implements ValueEventListener{

    private static final int NUMBER_COUNT = 25;
    private static final String TAG = BingoActivity.class.getSimpleName();
    private static final int LOW_BINGO_AMOUNT = 1;
    private String roomId;
    private RecyclerView recyclerView;
    private TextView textView;
    private Boolean creator;
    private List<Integer> randomNumbers;
    private List<NumberButton> buttons;
    private Map<Integer, NumberButton> buttonMap = new HashMap<>();
    private FirebaseRecyclerAdapter<Boolean, NumberButtonHolder> adapter;
    private int bingoAmount;
    private AlertDialog alertDialog;
    private FirebaseDBHelper firebaseDBHelper;
    private boolean myTurn = false;
    private DatabaseReference statusRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bingo);
        roomId = getIntent().getStringExtra("ROOM_ID");
        creator = getIntent().getBooleanExtra("IS_CREATOR", false);
        firebaseDBHelper = FirebaseDBHelper.getInstance();
        statusRef = FirebaseDatabase.getInstance().getReference("rooms").child(roomId).child("status");
        getRandomNumberButtons();
        if (isCreator()) {
            for (int i = 0; i < NUMBER_COUNT; i++) {
                firebaseDBHelper.setRoomsValue(roomId, "numbers", i+1, false);
            }
            firebaseDBHelper.setRoomsValue(roomId, "status", GameRoom.STATUS_CREATED);
        } else {
            //for joiner
            firebaseDBHelper.setRoomsValue(roomId, "status", GameRoom.STATUS_JOINED);
        }
        findViews();
    }

    private boolean checkBingo() {
        //bingo
        int[] nums = new int[NUMBER_COUNT];
        for (int i = 0; i < NUMBER_COUNT; i++) {
            nums[i] = buttons.get(i).isPicked() ? 1 : 0;
        }
        bingoAmount = 0;
        int sum = 0;
        for (int i = 0; i < 5; i++) {
            sum = 0;
            for (int j = 0; j < 5; j++) {
                sum += nums[i * 5 + j];
            }
            bingoAmount += (sum == 5) ? 1 : 0;
            sum = 0;
            for (int j = 0; j < 5; j++) {
                sum += nums[i + j * 5];
            }
            bingoAmount += (sum == 5) ? 1 : 0;
        }
        sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += nums[i*5+i];
        }
        bingoAmount += (sum == 5) ? 1 : 0;
        sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += nums[(i+1)*4];
        }
        bingoAmount += (sum == 5) ? 1 : 0;
        Log.d(TAG, "onChildChanged bingo: " + bingoAmount);
        if (bingoAmount > LOW_BINGO_AMOUNT) {
            firebaseDBHelper.setRoomsValue(roomId, "status", GameRoom.STATUS_BINGO);
            return true;
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
        statusRef.addValueEventListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
        statusRef.removeEventListener(this);
        FirebaseDatabase.getInstance().getReference("rooms")
                .child(roomId)
                .removeValue();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        if(dataSnapshot.getValue()==null){
            if(alertDialog==null||!alertDialog.isShowing()){
                showBingoDialog(getString(R.string.exit));
            }
            return;
        }
        long status = (long) dataSnapshot.getValue();
        switch ((int) status) {
            case GameRoom.STATUS_CREATED:
                textView.setText(R.string.wait_joiner);
                break;
            case GameRoom.STATUS_JOINED:
                textView.setText(R.string.start);
                setMyTurn(isCreator() ? true : false);
                firebaseDBHelper.setRoomsValue(roomId, "status", GameRoom.STATUS_CREATOR_TURN);
                break;
            case GameRoom.STATUS_CREATOR_TURN:
                setMyTurn(isCreator() ? true : false);
                break;
            case GameRoom.STATUS_JOINER_TURN:
                setMyTurn(!isCreator() ? true : false);
                break;
            case GameRoom.STATUS_BINGO:
                if(alertDialog==null||!alertDialog.isShowing()){
                    if(bingoAmount>LOW_BINGO_AMOUNT) {
                        showBingoDialog(getString(R.string.bingo_win));
                    }else{
                        showBingoDialog(getString(R.string.bingo_lose));
                    }
                }
                break;
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    class NumberButtonHolder extends RecyclerView.ViewHolder {
        NumberButton button;

        public NumberButtonHolder(View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.number_button);
        }
    }

    private void getRandomNumberButtons() {
        randomNumbers = new ArrayList<>();
        for (int i = 0; i < NUMBER_COUNT; i++) {
            randomNumbers.add(i + 1);
        }
        Collections.shuffle(randomNumbers);
        buttons = new ArrayList<>();
        for (int i = 0; i < NUMBER_COUNT; i++) {
            NumberButton button = new NumberButton(this);
            button.setText(String.valueOf(randomNumbers.get(i)));
            button.setNumber(randomNumbers.get(i));
            button.setPosition(i);
            buttons.add(button);
            buttonMap.put(button.getNumber(), button);
        }
    }

    public Boolean isCreator() {
        return creator;
    }

    public void setCreator(Boolean creator) {
        this.creator = creator;
    }

    private void findViews() {
        recyclerView = findViewById(R.id.bingo_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        textView = findViewById(R.id.bingo_tv);
        //RecyclerView
        Query query = FirebaseDatabase.getInstance().getReference("rooms")
                .child(roomId)
                .child("numbers")
                .orderByKey();
        FirebaseRecyclerOptions<Boolean> options = new FirebaseRecyclerOptions.Builder<Boolean>()
                .setQuery(query, Boolean.class).build();
        adapter = new FirebaseRecyclerAdapter<Boolean, NumberButtonHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull NumberButtonHolder holder, final int position, @NonNull Boolean model) {
                holder.button.setText(String.valueOf(buttons.get(position).getNumber()));
                holder.button.setEnabled(!buttons.get(position).isPicked());
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isMyTurn()) {
                            int number = buttons.get(position).getNumber();
                            firebaseDBHelper.setRoomsValue(roomId, "numbers", number, true);
                        }
                    }
                });
            }

            @Override
            public void onChildChanged(@NonNull ChangeEventType type, @NonNull DataSnapshot snapshot, int newIndex, int oldIndex) {
                super.onChildChanged(type, snapshot, newIndex, oldIndex);
                Log.d(TAG, "onChildChanged: " + type.name() + "/" + snapshot.getKey());
                if (type == ChangeEventType.CHANGED) {
                    NumberButton button = buttonMap.get(Integer.parseInt(snapshot.getKey()));
                    int pos = button.getPosition();
                    NumberButtonHolder numberButtonHolder = (NumberButtonHolder) recyclerView.findViewHolderForAdapterPosition(pos);
                    numberButtonHolder.button.setEnabled(false);
                    button.setPicked(true);
                    if (!checkBingo()&&isMyTurn()) {
                        firebaseDBHelper.setRoomsValue(roomId, "status",
                                isCreator() ? GameRoom.STATUS_JOINER_TURN : GameRoom.STATUS_CREATOR_TURN);
                    }
                }
            }

            @NonNull
            @Override
            public NumberButtonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(BingoActivity.this).inflate(R.layout.single_button, parent, false);
                return new NumberButtonHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
    }

    public boolean isMyTurn() {
        return myTurn;
    }

    public void setMyTurn(boolean myTurn) {
        this.myTurn = myTurn;
        textView.setText(myTurn ? getString(R.string.my_turn) : getString(R.string.others_turn));
    }

    private void showBingoDialog(String result) {
        textView.setText("");
        alertDialog = new AlertDialog.Builder(BingoActivity.this)
                .setTitle(R.string.app_name)
                .setMessage(result)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }
}
