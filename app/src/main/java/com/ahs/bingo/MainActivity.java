package com.ahs.bingo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

public class MainActivity extends BaseActivity implements FirebaseAuth.AuthStateListener, View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 5566;
    private FirebaseAuth auth;
    private TextView nickText;
    private ImageView personIcon;
    private Group groupIcon;
    int[] iconIds = {R.drawable.iconfinder_icons_user2, R.drawable.iconfinder_male3, R.drawable.iconfinder_female1};
    private Member member;
    private MainViewModel mainViewModel;
    private FirebaseDBHelper firebaseDBHelper;
    private FirebaseRecyclerAdapter<GameRoom, RoomViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        auth = FirebaseAuth.getInstance();
        bindViewModel();
        firebaseDBHelper = FirebaseDBHelper.getInstance();
    }

    private void bindViewModel() {
        // Get the ViewModel.
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // Create the observer which updates the UI.
        Observer<String> nameObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                nickText.setText(s);
            }
        };
        Observer<Integer> iconObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                personIcon.setImageResource(iconIds[integer]);
            }
        };
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        mainViewModel.getNickname().observe(this, nameObserver);
        mainViewModel.getIconId().observe(this, iconObserver);
    }

    private void findViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String roomName = nickText.getText()+"'s room";
                GameRoom gameRoom = new GameRoom(roomName, member);
                String key = firebaseDBHelper.initRooms(gameRoom);
                Intent intent = new Intent(MainActivity.this, BingoActivity.class);
                intent.putExtra("ROOM_ID", key);
                intent.putExtra("IS_CREATOR", true);
                startActivity(intent);
            }
        });
        nickText = findViewById(R.id.tv_nickname);
        personIcon = findViewById(R.id.personIcon);
        groupIcon = findViewById(R.id.group_icon);
        groupIcon.setVisibility(View.GONE);
        personIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean visible = groupIcon.getVisibility() == View.GONE ? false : true;
                groupIcon.setVisibility(visible ? View.GONE : View.VISIBLE);
            }
        });
        findViewById(R.id.icon_0).setOnClickListener(this);
        findViewById(R.id.icon_1).setOnClickListener(this);
        findViewById(R.id.icon_2).setOnClickListener(this);
        //Recycler
        RecyclerView recyclerView = findViewById(R.id.room_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Query query = FirebaseDatabase.getInstance().getReference("rooms").limitToLast(30);
        FirebaseRecyclerOptions<GameRoom> options = new FirebaseRecyclerOptions.Builder<GameRoom>()
                .setQuery(query, GameRoom.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<GameRoom, RoomViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RoomViewHolder holder, int position, @NonNull final GameRoom model) {
                holder.imageView.setImageResource(iconIds[model.getFirst().getIconId()]);
                holder.textView.setText(model.getName());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "onClick: " + model.getId());
                        Intent intent = new Intent(MainActivity.this, BingoActivity.class);
                        intent.putExtra("ROOM_ID", model.getId());
                        intent.putExtra("IS_CREATOR", false);
                        startActivity(intent);
                    }
                });
            }
            @NonNull
            @Override
            public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(MainActivity.this)
                        .inflate(R.layout.room_row, parent, false);
                return new RoomViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public RoomViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.room_image);
            textView = itemView.findViewById(R.id.room_text);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(this);
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        auth.removeAuthStateListener(this);
        adapter.stopListening();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_signout:
                auth.signOut();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        Log.d(TAG, "onAuthStateChanged: ");
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            final String displayName = user.getDisplayName();
            String uid = user.getUid();
            firebaseDBHelper.setUsersValue(uid, "displayName", displayName);
            firebaseDBHelper.setUsersValue(uid, "uid", uid);
            FirebaseDatabase.getInstance()
                    .getReference("users")
                    .child(uid)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            member = (Member) dataSnapshot.getValue(Member.class);
                            if (member.getNickName() != null) {
                                mainViewModel.getNickname().postValue(member.getNickName());
                            } else {
                                addNicknameDialog(displayName);
                            }
                            mainViewModel.getIconId().postValue(member.getIconId());
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.GoogleBuilder().build(),
                                    new AuthUI.IdpConfig.EmailBuilder().build()))
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN
            );
        }

    }

    private void addNicknameDialog(String dsiplayName) {
        final EditText editText = new EditText(this);
        editText.setText(dsiplayName);
        new AlertDialog.Builder(this)
                .setTitle("Nick name")
                .setMessage("Please enter your nick name")
                .setView(editText)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String nickName = editText.getText().toString();
                        firebaseDBHelper.setUsersValue(auth.getUid(), "nickName", nickName);
                    }
                })
                .show();
    }

    public void changeNickname(View view) {
        addNicknameDialog(nickText.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if (view instanceof ImageView) {
            int selectIconId = 0;
            switch (view.getId()) {
                case R.id.icon_1:
                    selectIconId = 1;
                    break;
                case R.id.icon_2:
                    selectIconId = 2;
                    break;
            }
            firebaseDBHelper.setUsersValue(auth.getCurrentUser().getUid()
                    , "iconId", selectIconId);
            groupIcon.setVisibility(View.GONE);
        }
    }
}
