package com.ahs.bingo;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDBHelper {
    private static FirebaseDBHelper firebaseDBHelper;

    public static FirebaseDBHelper getInstance() {
        if (firebaseDBHelper == null) {
            firebaseDBHelper = new FirebaseDBHelper();
        }
        return firebaseDBHelper;
    }

    public void setUsersValue(String uid, String child, Object value){
        FirebaseDatabase.getInstance()
                .getReference("users")
                .child(uid)
                .child(child)
                .setValue(value);
    }

    public String initRooms(GameRoom gameRoom) {
        DatabaseReference rooms = FirebaseDatabase.getInstance().getReference("rooms");
        DatabaseReference roomRef = rooms.push();
        roomRef.setValue(gameRoom);
        String key = roomRef.getKey();
        roomRef.child("id").setValue(key);
        return key;
    }

    public void setRoomsValue(String roomId, String child, int i, Object value) {
        FirebaseDatabase.getInstance().getReference("rooms")
                .child(roomId)
                .child(child)
                .child(String.valueOf(i))
                .setValue(value);
    }

    public void setRoomsValue(String roomId, String child, Object value) {
        FirebaseDatabase.getInstance().getReference("rooms")
                .child(roomId)
                .child(child)
                .setValue(value);
    }
}
