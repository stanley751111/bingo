package com.ahs.bingo;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class BaseActivity extends AppCompatActivity{

    private static final String TAG = BaseActivity.class.getSimpleName();
    private AlertDialog alertDialog;
    private ConnectivityManager cm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        ckeckNetwork();
    }

    @Override
    protected void onStart() {
        super.onStart();
        cm.registerDefaultNetworkCallback(networkCallback);
    }

    @Override
    protected void onStop() {
        super.onStop();
        cm.unregisterNetworkCallback(networkCallback);
    }

    private void ckeckNetwork() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(!isConnected){
            showRequestNetworkDialog();
        }
    }

    private void showRequestNetworkDialog() {
        alertDialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.app_name))
                .setMessage(R.string.setup_network)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .setCancelable(false)
                .show();
    }

    ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback(){
        public void onAvailable(Network network) {
            Log.d(TAG, "onAvailable: "+network.toString());
            if(alertDialog!=null&&alertDialog.isShowing()){
                alertDialog.dismiss();
            }
        }

        public void onLosing(Network network, int maxMsToLive) {
            Log.d(TAG, "onLosing: "+network.toString());
            showRequestNetworkDialog();
        }

        public void onLost(Network network) {
            Log.d(TAG, "onLost: "+network.toString());
            showRequestNetworkDialog();
        }

        public void onUnavailable() {
            Log.d(TAG, "onUnavailable:");
            showRequestNetworkDialog();
        }
    };
}
