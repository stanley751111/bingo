package com.ahs.bingo;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {
    private MutableLiveData<String> nickName;
    private MutableLiveData<Integer> iconId;

    public MutableLiveData<String> getNickname() {
        if (nickName == null) {
            nickName = new MutableLiveData<>();
        }
        return nickName;
    }

    public MutableLiveData<Integer> getIconId() {
        if (iconId == null) {
            iconId = new MutableLiveData<>();
        }
        return iconId;
    }
}
