package com.ahs.bingo;

public class GameRoom {
    public static final int STATUS_NOT_READY=0;
    public static final int STATUS_CREATED = 1;
    public static final int STATUS_JOINED = 2;
    public static final int STATUS_CREATOR_TURN = 3;
    public static final int STATUS_JOINER_TURN = 4;
    public static final int STATUS_BINGO = 7;
    //public static final int STATUS_CREATOR_WIN = 5;
    //public static final int STATUS_JOINER_WIN = 6;


    String id;
    String name;
    Member first;
    Member join;
    int status;

    public GameRoom(){

    }

    public GameRoom(String name, Member first) {
        this.name = name;
        this.first = first;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Member getFirst() {
        return first;
    }

    public void setFirst(Member first) {
        this.first = first;
    }

    public Member getJoin() {
        return join;
    }

    public void setJoin(Member join) {
        this.join = join;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
